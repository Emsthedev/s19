console.log("Discussion");
console.log("");
/*  */

// alert("Hello World");

/*
	Selection Control Structures
		- it sorts out whether the statement/s are to be executed based on the condition whether it is true or false
		- two-way selection (true or false)
		- multi-way selection
*/

// IF ELSE STATEMENTS

/*
	Syntax:
	 if(condition) {
		// statement
	 } else if (condition) {
		// statement
	 } else {
		// statement
	 }
*/

/*
	if statement - Executes a statement if a specified condition is true
*/

let numA = -1;

if(numA < 0) {
	console.log("Hello");
}

console.log(numA < 0);

let city = "New York";

if(city === "New York") {
	console.log("Welcome to New York City!");
}

/*
	Else if 
		- executes a statement if our previous conditions are false and if the specified condition is true
		- the "else if" clause is option and can be added to capture additional conditions to change the flow of a program
	
*/

let numB = 1;

if (numA > 0) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
};

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City!");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo!")
};

/*
	else statement
		- executes a statement if all of our prevouis conditions are false
*/

if (numA > 0) {
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log("Again");
};

/* let age = parseInt(prompt("Enter your age:"));

if(age <= 18) {
	console.log("Not allowed to drink");
} else{
	console.log("Congrats! You can now take a shot!")
}; */


// Mini-Activity
    let height = 151;

    if(height <= 150){
        console.log("did not passed the min height requirement")
    }

    else{
        console.log("Passed the min height requirement")
    }


    //Nested if statement

let isLegalAge = true;
let isAdmin = false;

if(isLegalAge){
            if(!isAdmin){
                console.log("You are not an admin")
            }
}


/* 


*/

    let message = "No message";

    function determineTyphoonIntensity(windSpeed){

            if (windSpeed < 30) {
                return 'Not a typhoon';
            }

            else if(windSpeed <= 61){
                    return 'Tropical depression detected';
            }

            else if(windSpeed >= 62 && windSpeed <= 88){
                return 'Tropical Storm Detected';

            }

            else if(windSpeed >= 89 && windSpeed <= 177){
                return 'Severe Tropical Storm Detected';

            }
            else {
                return 'typhoon detected'
            }

    }

    message = determineTyphoonIntensity(70);

    console.log(message);

    message = determineTyphoonIntensity(178);

    console.log(message);


    console.log("");


    /* 
        Truthy and Falsy
    */


        /* 
        In JS a truthy value is a value that is
        considered true when encountered in a boolean
        context.
        
        
        Falsy Values:
        1.false
        2.0
        3.-0
        4.""
        5. null
        6. undefined
        7.NaN
        
        */


//Truthy Examples:

let word = "true";

if (word){
    console.log("Truthy");
}

if (true){
    console.log("Truthy");
}

if (1){
    console.log("Truthy");
}

if([]){
    console.log("Truthy");
}

//Falsy Example: (wont show);

console.log("");

if(false){
    console.log("Falsy");
}

if(0){
    console.log("Falsy");
}

if(-0){
    console.log("Falsy");
}

if(""){
    console.log("Falsy");
}

if(undefined){
    console.log("Falsy");
}

if(null){
    console.log("Falsy");
}

if(NaN){
    console.log("Falsy");
}


/*  Conditional (Ternanry ) Operator = short code*/

console.log("");
/* let ternaryResult = (1<18) ? "Condition is True" : 
                             "Condition is False"

 console.log("Result of Ternary Operator" + ternaryResult);
 console.log("");

 let name;

 function isOfLegalAge(){
    name = 'John'
    return 'You are of Legal age limit'
 }


 function isUnderAge(){
    name = 'Jane'
    return 'You are under of Legal age limit'
 }

 let yourAge = parseInt(prompt("What is your age? "));
 console.log(yourAge);
 let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();

 console.log("Result of Ternary Operator in Function:" + legalAge + ',' + name);
 */
/* Switch Statements */

/* let day = prompt("what day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case  'monday':
        console.log(" color of the day red");
        break;
 
    case 'tuesday':
        console.log(" color of the day blue");
        break;
    case 'wednesday':
        console.log(" color of the day yellow");
        break;
    case 'thursday':
        console.log(" color of the day turquoise");
        break;
    case 'friday':
        console.log(" color of the day brown");
        break;

    case 'saturday':
        console.log(" color of the day orange");
        break;

    case 'sunday':
        console.log(" color of the day green");
        break;

    default:
        console.log(" pleaseinput a valid day");
        break;
}
 */
/* Try Catche */


console.log("");

function showIntensityAlert(windSpeed){
    try {
        alerat(determineTyphoonIntensity(windSpeed))
    }

    catch (error) {
        console.log(typeof error)
        console.log(error)
        console.warn(error.message)
    }

    finally {
        alert ("Intensity updates will show new alert")
    }
}

showIntensityAlert(56);


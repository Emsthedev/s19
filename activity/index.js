console.log("");


// S19 - Javascript - Selection Control Structures

/*ACTIVITY*/


/*

	1. Declare 3 global variables without initialization called username,password and role.
	2. Create a login function which is able to prompt the user to provide their username, password and role.
		a.) use prompt() and update the username,password and role global variables with the prompt() returned values.
		b.) add an if statement to check if the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
			** if it is, show an alert to inform the user that their input should not be empty.
		c.) Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
			** if the user's role is admin, show an alert with the following message:
						"Welcome back to the class portal, admin!"
			** if the user's role is teacher, show an alert with the following message:
						"Thank you for logging in, teacher!"
			** if the user's role is a student, show an alert with the following message:
 						"Welcome to the class portal, student!"
			** if the user's role does not fall under any of the cases, as a default, show a message:
 						"Role out of range."
*/


	// Code here:
    let userName;
    let password;
    let hasRole;

    function account() {
        userName = prompt("Please enter your username");
        password = prompt("Please enter your password");
        hasRole = prompt("Please enter your access role ex. (admin, teacher, student)");

        if (!userName) {
            alert("Username is empty");
        } else if (!password) {
            alert("Password is empty");
        } else if (!hasRole) {
            alert("Role is empty");
        } else {
            switch (hasRole) {
                case "student":
                    alert("Welcome to the class portal, " + hasRole + "!");
                    break;
                case "teacher":
                    alert("Thank you for logging in, " + hasRole + "!");
                    break;

                case "admin":
                    alert("Welcome back to the class portal, " + hasRole + "!");
                    break;


                default:
                    alert("Role out of range");
                    break;
            }
        }
    }

    account();
    



/*
	3. Create a function which is able to receive 4 numbers as arguments, calculate its average and log a message for  the user about their letter equivalent in the console.
		a.) add parameters appropriate to describe the arguments.
		b.)create a new function scoped variable called average.
		c.) calculate the average of the 4 number inputs and store it in the variable average.
		d.)research the use of Math.round() and round off the value of the average variable.
			**update the average variable with the use of Math.round()
			**console.log() the average variable to check if it is rounding off first.

	4. add an if statement to check if the value of avg is less than or equal to 74.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is F"

	5. add an else if statement to check if the value of avg is greater than or equal to 75 and if average is less than or equal to 79.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is D"

	6. add an else if statement to check if the value of avg is greater than or equal to 80 and if average is less than or equal to 84.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is C"

	7. add an else if statement to check if the value of avg is greater than or equal to 85 and if average is less than or equal to 89.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is B"

	8. add an else if statement to check if the value of avg is greater than or equal to 90 and if average is less than or equal to 95.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A"

	9. add an else if statement to check if the value of average is greater than equals to 96.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A+"

*/


	// Code here:
  
   let inputGrade1;
   let inputGrade2;
   let inputGrade3;
   let inputGrade4;

   function gradesInput() {
       inputGrade1 = parseInt(prompt("What is your 1st Grade Score?: "));
       inputGrade2 = parseInt(prompt("What is your 2nd Grade Score?: "));
       inputGrade3 = parseInt(prompt("What is your 3rd Grade Score?: "));
       inputGrade4 = parseInt(prompt("What is your 4th Grade Score?: "));

       let average = Math.round((inputGrade1 + inputGrade2 + inputGrade3 + inputGrade4) / 4);
       console.log("Your Average Grade is: " + average);


       if (average <= 74) {
           console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
       } else if (average >= 75 && average <= 79) {
           console.log("Hello, student, your average is " + average + ". The letter equivalent is D");

       } else if (average >= 80 && average <= 84) {
           console.log("Hello, student, your average is " + average + ". The letter equivalent is C");

       } else if (average >= 85 && average <= 89) {
           console.log("Hello, student, your average is " + average + ". The letter equivalent is B");

       } else if (average >= 90 && average <= 95) {
           console.log("Hello, student, your average is " + average + ". The letter equivalent is A");

       } else if (average >= 96) {
           console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
       }
   }
   gradesInput();
   
    

	

    


   